import React from 'react';

const Button = ({ children, onPress, color, withShadow }) => {

  return (
    <div>
      <button style={buttonBase}>
        {children}
      </button>
    </div>
  );
};
