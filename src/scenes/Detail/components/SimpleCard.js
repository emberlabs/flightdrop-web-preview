import React from 'react';

const SimpleCard = ({ uri, title }) => {
  return (
    <div className="simpleCardContainer">
      <div className="imageContainer">
        <img src={uri} className="image" alt={title}/>
      </div>
      <p className="simpleCardText">{title}</p>
    </div>
  );
};


export default SimpleCard;
