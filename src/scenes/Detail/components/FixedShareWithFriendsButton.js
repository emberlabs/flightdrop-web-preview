import React from 'react';
import { Dimensions, View, Animated } from 'react-native';
import { Button, AppText } from '../../common';
import {
  COLOR_GRADIENT_YELLOW,
  FONT_BOLD,
} from '../../../services/constants/styles';

const FixedShareWithFriendsButton = ({ toggleShareScreen, shareButtonAnim }) => {
  const { buttonContainer, buttonText } = styles;
  return (
      <Animated.View style={[buttonContainer, {transform: [{ translateY: shareButtonAnim }]}]}>
        <Button
          color={COLOR_GRADIENT_YELLOW}
          onPress={toggleShareScreen}
          withShadow
        >
          <AppText size="md" style={buttonText}>
            Share with Friends
          </AppText>
        </Button>
      </Animated.View>
  );
};

const styles = {
  buttonContainer: {
    position: 'absolute',
    width: 300,
    bottom: 20,
    height: 90,
    left: Dimensions.get('window').width / 2 - 150,
    paddingHorizontal: 50,
    paddingVertical: 20,
  },
  buttonText: {
    color: 'white',
    fontFamily: FONT_BOLD,
    fontSize: 18,
  },
};

export default FixedShareWithFriendsButton;
