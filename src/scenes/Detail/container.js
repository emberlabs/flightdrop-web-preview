import React, { Component } from 'react';
import axios from 'axios';
import Detail from './index';

class DetailContainer extends Component {
  state = { selectedDeal: {}, loaded: false, scrollPos: 0 };

  componentDidMount() {
    axios
      .get('http://165.227.179.20:3000/deals/5b0d58a3cf287215985e4eba')
      .then((res) => {
        this.setState({ selectedDeal: res.data, loaded: true });
      })
      .catch(function(error) {
        console.log('user/deals error',error);
      });
  }

  render() {
    if(!this.state.loaded) {
      return <div/>;
    }
    return (
        <Detail
          selectedDeal={this.state.selectedDeal}
          scrollPos={this.state.scrollPos}
        />
    );
  }
}

export default DetailContainer;
