import React from 'react';
import PropTypes from 'prop-types';

const BookSection = ({ regularPrice, dealPrice }) => {
  return (
    <div className="bookSection">
      <div className="priceContainer">
        <p className="priceLabel">Regular</p>
        <p className="regularPriceStyle">${regularPrice}</p>
      </div>

      <div className="priceContainer">
        <p className="priceLabel">Now</p>
        <p className="dealPriceStyle">${dealPrice}</p>
      </div>
      <div className="bookButtonContainer">
        <div className="button red">
          <p className="bookButtonText">Book Now</p>
        </div>
      </div>
    </div>
  );
};

BookSection.propTypes = {
  regularPrice: PropTypes.number.isRequired,
  dealPrice: PropTypes.number.isRequired,
};

export default BookSection;
