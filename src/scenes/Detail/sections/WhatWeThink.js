import React from 'react';
const WhatWeThink = () => {
  return (
    <div className="whatWeThink-container">
      <p className="headText">What We Think</p>
      <p className="text">
        This is a great (and rare) deal to the “Las Vegas of the East”. These
        flights are on five-star airline EVA Air. No bag fees, etc.{' '}
      </p>
    </div>
  );
};

export default WhatWeThink;
