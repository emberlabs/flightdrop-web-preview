import React from 'react';

const Rating = () => {
    return (
      <div className="rating-container">
        <p className="headText">Rate this Deal</p>
        <p className="text">Rating helps us send you more relevant deals.</p>
        <div className="starContainer">
          <img className="star" src={require('../../../images/star.svg')} alt=""/>
          <img className="star" src={require('../../../images/star.svg')} alt=""/>
          <img className="star" src={require('../../../images/star.svg')} alt=""/>
          <img className="star" src={require('../../../images/star.svg')} alt=""/>
          <img className="star" src={require('../../../images/star.svg')} alt=""/>
        </div>
      </div>
    );
}

export default Rating;
