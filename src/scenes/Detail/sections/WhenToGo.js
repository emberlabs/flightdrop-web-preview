import React from 'react';

const WhenToGo = () => {
  return (
    <div className="whenToGo-container">
      <p className="headText">When to Go</p>
      <p className="text">Between February - June this year. Check out example dates below:</p>
    </div>
  );
};

export default WhenToGo;
