import React from 'react';

const LastBooking = () => {
  return (
    <div className="lastBooking-container">
      <p className="headText">Did you book?</p>
      <p className="text">
        Congrats! Submit a screenshot of your itinery to receive up to 500
        miles!
      </p>
      <div className="buttonContainer">
        <div className="buttonWrapper">
          <div className="button">
            <p className="buttonText">Submit itinery</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default LastBooking;
