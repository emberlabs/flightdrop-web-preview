import React from 'react';

const ShareToFriend = () => {
  return (
    <div className="ShareToFriend-container">
      <p className="headText">Share this Deal</p>
      <div className="buttonContainer">
        <div className="buttonWrapper">
          <div className="button purple">
            <img className="icon" src={require('../../../images/sent-mail.svg')} alt=""/>
            <p className="buttonText">Send to a friend</p>
          </div>
        </div>
      </div>
    </div>
  );
};

export default ShareToFriend;
