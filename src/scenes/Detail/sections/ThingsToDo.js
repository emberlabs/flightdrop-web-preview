import React from 'react';
import SimpleCard from '../components/SimpleCard';

const fakeData = [
  {
    id: 1,
    uri: 'https://farm1.staticflickr.com/902/27198669248_fd5c1bea45_b.jpg',
    title: 'The Venetian Macao',
  },
  {
    id: 2,
    uri: 'https://farm1.staticflickr.com/902/27198669248_fd5c1bea45_b.jpg',
    title: 'The Venetian Macao',
  },
  {
    id: 3,
    uri: 'https://farm1.staticflickr.com/902/27198669248_fd5c1bea45_b.jpg',
    title: 'The Venetian Macao',
  },
  {
    id: 4,
    uri: 'https://farm1.staticflickr.com/902/27198669248_fd5c1bea45_b.jpg',
    title: 'The Venetian Macao',
  },
  {
    id: 5,
    uri: 'https://farm1.staticflickr.com/902/27198669248_fd5c1bea45_b.jpg',
    title: 'The Venetian Macao',
  },
];

const renderSimpleCard = things => {
  return things.map(thing => {
    return <SimpleCard key={thing.id} uri={thing.uri} title={thing.title} />;
  });
};

const ThingsToDo = () => {
  return (
    <div className="thingsToDo-container">
      <p className="headText">
        Things to Do
      </p>
      <div className="sliderDock-container" >
        <div className="sliderDock" >
          {renderSimpleCard(fakeData)}
        </div>
      </div>
    </div>
  );
};

export default ThingsToDo;
