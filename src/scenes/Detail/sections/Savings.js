import React from 'react';

const Saving = () => {
  const percentage = 90;
  const totalAmount = 700;
  return (
    <div className="saving-container">
      <p className="headText">Your Savings</p>
      <div className="savingAmountContainer">
        <p className="savingAmountText" style={{ left: '75%'}}>
          ${totalAmount * percentage / 100}
        </p>
      </div>
      <div className="parameter-container">
        <div className="inside"/>
      </div>
      <div className="rulerContainer">
        <p className="rulerText">$300</p>
        <p className="rulerText">$400</p>
        <p className="rulerText">$500</p>
        <p className="rulerText">$600</p>
        <p className="rulerText">$700</p>
      </div>
    </div>
  );
};
export default Saving;
