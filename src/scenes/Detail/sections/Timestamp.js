import React from 'react';

const Timestamp = ({ diffTime }) => {
  let title, contents;
  if( diffTime < 5 ) {
    title = 'Just Dropped';
    contents = 'This deal is still hot! Grab it while you can, before prices jump back up again!';
  } else if (diffTime < 24 && diffTime >= 5) {
    title = 'Almost Gone';
    contents = 'This deal is selling out quick! Check all available dates and snatch it up before prices rise!';
  } else {
    title = 'Check Availability';
    contents = 'It’s been a while since this deal dropped. Check the calendar to see if prices are still low.';
  }

  const renderIcon = (title) => {
    return <img className="icons" src={require("../../../images/fire_emoji.png")}  alt={title}/>;
    // if( diffTime < 5 ) {
    //   return <img src="../../../images/fire_emoji.png" />;
    // } else if (diffTime < 24 && diffTime >= 5) {
    //   return <img src="../../../images/fire_emoji.png" />;
    // } else {
    //   return <img src="../../../images/fire_emoji.png" />;
    // }
  }

  return (
    <div className="timestamp-container">
      <div className="headContainer">
        { renderIcon(title) }
        <p className="headerText" size="lg">
          {title}
        </p>
      </div>
      <p className="text">{contents}</p>
    </div>
  );
};

export default Timestamp;
