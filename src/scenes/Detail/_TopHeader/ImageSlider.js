import React from 'react';

const ImageSlider = ({ src }) => {
  if (src.length > 0) {
    return (
      <div className="ImageSlider-container">
        <img src={ src[0] } alt={src[0]}/>
      </div>
    );
  }
  return <div className="ImageSlider-container" />;
};

export default ImageSlider;
