import React from 'react';
import ImageSlider from './ImageSlider';

const renderTags = tags => {
  return tags.map((tag, index) => {
    let styleColors;
    let text = tag.name;
    switch (text.toLowerCase()) {
      case 'nonstop':
        styleColors = 'nonstop';
        text='non-stop';
        break;
      case 'mistake fare':
        styleColors = 'mistake';
        break;
      default:
        styleColors = 'red';
    }
    return (
      <div key={index} className={'tag-container ' + styleColors}>
        <p className="tag-text">{text.toUpperCase()}</p>
      </div>
  );
  })
}

const TopHeader = ({ images, flightType, destination, departure, tags }) => {
  return (
    <div className="topImageContainer">
      <div className="topImageWrapper">
        <ImageSlider src={images} />
        <div className="innerShadow"/>
        <div className="cardTitleContainer">
          <p className="flightTypeStyle">{flightType}</p>
          <p className="flightDeparture">{departure} to</p>
          <p className="cardDestination">{destination}</p>
        </div>
        <div className="cardTagContainer">
          { renderTags(tags) }
        </div>
      </div>
    </div>
  );
};

export default TopHeader;
