import { Dimensions } from 'react-native';

export default {
  container: {
    flex: 1,
    position: 'absolute',
    bottom: 0,
    width: '100%',
    height: '100%',
    backgroundColor: 'white',
    justifyContent: 'flex-start',
    alignItems: 'center',
    shadowColor: '#000',
    shadowOffset: { width: 0, height: 7 },
    shadowOpacity: 0.8,
    shadowRadius: 20,
  },
  circleAvatorContainer: {
    position: 'absolute',
    left: Dimensions.get('window').width / 2 - 45,
  },
  emoji: {
    fontSize: 25,
  },
  title: {
    marginTop: 50,
  },
  contents: {
    marginTop: 10,
    textAlign: 'center',
    marginBottom: 10,
    paddingHorizontal: 15,
    lineHeight: 20,
  },
};
