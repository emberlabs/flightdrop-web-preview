import React from 'react';
import { View, Text } from 'react-native';
import { CircleAvator, AppText, ShareFriendsIcons } from '../../common/index';
import styles from './styles';

const ShareWithFriends = ({handleShare}) => {
  const { container, circleAvatorContainer, emoji, title, contents } = styles;
  return (
    <View style={{ flex: 1, backgroundColor: 'transparent', paddingTop: 50 }}>
      <View style={container}>
        <AppText style={title} size="lg">
          Share with Friends
        </AppText>
        <AppText style={contents}>
          Save HUGE when you make this trip with friends or family. You'll also
          level up when they join!
        </AppText>
        <ShareFriendsIcons handleShare={handleShare}/>
      </View>
      <View style={circleAvatorContainer}>
        <CircleAvator width={90}>
          <Text style={emoji}>✈️</Text>
          <Text style={emoji}>🌏</Text>
        </CircleAvator>
      </View>
    </View>
  );
};

export default ShareWithFriends;
