import React from 'react';
import TopHeader from './_TopHeader';
import PlaneLine from './PlaneLine';
import { capitalizeFirstLetter } from '../../utils/helperFunc';
import BookSection from './sections/Booking';
import DetailSection from './DetailSection';
import TimeStamp from './sections/Timestamp';
import Savings from './sections/Savings';
import WhatWeThink from './sections/WhatWeThink';
import ThingsToDo from './sections/ThingsToDo';
import WhenToGo from './sections/WhenToGo';
import Rating from './sections/Rating';
import LastBooking from './sections/LastBooking';
import ShareToFriend from './sections/ShareToFriend';

const Detail = ({selectedDeal, scrollPos}) => {
    const tags = selectedDeal.tags.filter(tag => tag.name && !tag.emoji);
    return(
      <div className="Detail-container">
        <TopHeader
          images={selectedDeal.toLocations[0].images}
          flightType={capitalizeFirstLetter(selectedDeal.type)}
          destination={selectedDeal.to[0]}
          departure={selectedDeal.from[0]}
          tags={tags}
          isFavorite={true}
        />
        {/*<div className="planeLineContainer">*/}
          {/*<div className="verLine" />*/}
        {/*</div>*/}
        <PlaneLine
          scrollPos={scrollPos}
          totalMiles={30}
          shouldShowMiles={false}
          bottomPos={200}
        />
        <div className="contentsContainer">
          <BookSection
            regularPrice={550}
            dealPrice={selectedDeal.price}
          />
          <DetailSection
            component={<TimeStamp diffTime={-12} />}
          />
          <DetailSection component={<Savings />}/>
          <DetailSection component={<WhatWeThink />}/>
          <DetailSection component={<ThingsToDo />} />
          <DetailSection component={<WhenToGo />}/>
          <DetailSection component={<Rating/>}/>
          <DetailSection component={<LastBooking />}/>
          <DetailSection component={<ShareToFriend />}/>
        </div>
      </div>
    )
}

export default Detail;
