import React from 'react';

const DetailSection = ({component}) => {
  return (
    <div className="dotMark-container">
      <div className="dotMark"></div>
      {component}
    </div>
  );
}

export default DetailSection;
