import React, { Component } from 'react';

// let posOutOfBox = 0;

class PlaneLine extends Component {
  // componentDidMount() {
  //   if(this.props.prevMiles > 0) {
  //     posOutOfBox = this.props.prevMiles;
  //   }
  //   this.props.scrollPos.addListener(this.onScroll.bind(this));
  // }
  //
  // componentWillUnmount() {
  //   this.props.saveMiles(posOutOfBox);
  // }
  //
  // onScroll = (pos) => {
  //   if(this.props.shouldShowMiles) {
  //     const currentMile = Math.floor(pos.value / this.props.bottomPos * this.props.totalMiles);
  //     if(posOutOfBox < currentMile) {
  //       posOutOfBox = currentMile;
  //       this._textInput.setNativeProps({text: `+ ${posOutOfBox}`});
  //     }
  //   }
  // }

  render() {
    const { scrollPos, prevMiles, shouldShowMiles, } = this.props;
    console.log('scrollPos', scrollPos);
    return (
      <div className="planeLineContainer">
        <div className="verLine" />
        <div
          className="verLineInside" style={{ transform: `translateY(${scrollPos}px)` }}
        />
        <div
          className="planeContainer" style={{ transform: `translateY(${scrollPos}px)` }}
        >
          <img src={require('../../images/flight_emoji.png')} className="plane" />
        </div>
        { shouldShowMiles && (
          <div
            className="textContainer" style={{ transform: `translateY(${scrollPos})` }}
          >
            <input
              editable={false}
              underlineColorAndroid='rgba(0, 0, 0, 0)'
              className="text"
              defaultValue={`+${prevMiles}`}
              ref={component => this._textInput = component}
            />
          </div>
        )}
      </div>
    );
  }
};

// const planeOffset = 40;
//
// const classNames"= {
//   planeLineContainer: {
//     position: 'absolute',
//     top: 0,
//     left: 0,
//     width: 50,
//     height: 2000,
//     backgroundColor: 'transparent',
//   },
//   verLine: {
//     width: 2,
//     height: '100%',
//     position: 'relative',
//     backgroundColor: '#F3F3F3',
//     marginLeft: 20,
//   },
//   verLineInside: {
//     position: 'absolute',
//     top: '-100%',
//     left: 20,
//     width: 2,
//     height: '100%',
//     paddingTop: 20,
//     marginTop: planeOffset,
//     backgroundColor: COLOR_PRIMARY,
//   },
//   planeContainer: {
//     position: 'absolute',
//     left: 6,
//     top: -10,
//     marginTop: planeOffset,
//   },
//   plane: {
//     width: 30,
//     height: 30,
//   },
//   textContainer: {
//     position: 'absolute',
//     left: 24,
//     top: -25,
//     marginTop: planeOffset,
//   },
//   text: {
//     backgroundColor: 'rgba(0,0,0,0)',
//     color: COLOR_PRIMARY,
//     fontFamily: FONT_BOLD,
//     height: 35,
//     width: 35,
//     fontSize: 11
//   }
// };

export default PlaneLine;
