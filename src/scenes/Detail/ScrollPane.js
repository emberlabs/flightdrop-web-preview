import React, { Component } from 'react';
import PropTypes from 'prop-types';


export default class ScrollPane extends Component {

  static contextTypes = {
    registerPane: PropTypes.func.isRequired,
    unregisterPane: PropTypes.func.isRequired
  };

  componentDidMount() {
    this.context.registerPane(this.el)
  }

  componentWillUnmount() {
    this.context.unregisterPane(this.el)
  }

  render() {
    return (
      <div ref={(el) => { this.el = el }}>
        {this.props.children}
      </div>
    )
  }
}
