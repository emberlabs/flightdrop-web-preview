import React, { Component } from 'react';

class AppHeader extends Component {
  render() {
    return(
      <div className="appHeader-container">
        <img className="logo" src={require('../../images/logo.png')} alt=""/>
        <p className="head-text">Jack invited you to check out this Amazing deal!</p>
        <p className="text">This deal will most likely last 12-24 hours. Download Flightdrop for free to get more deals like this for 50-70% off regular price.</p>
        <div className="cta-container">
          <button className="appHeader-button">
            <p className="button-text">GET</p>
          </button>
          <div className="rating-container">
            <div className="stars-container">
              <img className="star-icon" src={require('../../images/star_white.svg')} alt=""/>
              <img className="star-icon" src={require('../../images/star_white.svg')} alt=""/>
              <img className="star-icon" src={require('../../images/star_white.svg')} alt=""/>
              <img className="star-icon" src={require('../../images/star_white.svg')} alt=""/>
              <img className="star-icon" src={require('../../images/star_white.svg')} alt=""/>
            </div>
            <p className="rating-text">4.9 rating</p>
          </div>
        </div>
      </div>
    )
  }
}

export default AppHeader
