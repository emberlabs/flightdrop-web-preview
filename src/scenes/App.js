import React, { Component } from 'react';
import AppHeader from './AppHeader';
import Detail from './Detail/container';

class App extends Component {
  render() {
    return (
      <div className="app-container">
        <AppHeader />
        <div className="dtail-overwrap">
          <Detail />
        </div>
      </div>
    );
  }
}

export default App;
